var kwaks = document.getElementsByClassName("kwak");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(kwaks, function (el) {
  el.addEventListener("click", sayKwak);
});

function sayKwak() {
  var audio = document.getElementById("kwak");
  audio.play();
}
//---------------------------------------------------------------
var miaus = document.getElementsByClassName("miau");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(miaus, function (el) {
  el.addEventListener("click", saymiau);
});

function saymiau() {
  var audio = document.getElementById("miau");
  audio.play();
}
//----------------------------------------------------------------
var miaus2 = document.getElementsByClassName("miau2");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(miaus2, function (el) {
  el.addEventListener("click", saymiau2);
});

function saymiau2() {
  var audio = document.getElementById("miau2");
  audio.play();
}

//-------------------------------------------------------------------
var cabras = document.getElementsByClassName("cabra");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(cabras, function (el) {
  el.addEventListener("click", saycabra);
});

function saycabra() {
  var audio = document.getElementById("cabra");
  audio.play();
}
//-------------------------------------------------------------------------

var angels = document.getElementsByClassName("angel");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(angels, function (el) {
  el.addEventListener("click", say_angel);
});

function say_angel() {
  var audio = document.getElementById("angel");
  audio.play();
}
//------------------------------------------------------------------------------
var xs = document.getElementsByClassName("x");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(xs, function (el) {
  el.addEventListener("click", sayx);
});

function sayx() {
  var audio = document.getElementById("x");
  audio.play();
}
//--------------------------------------------------------------------------------
var dafts = document.getElementsByClassName("daft");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(dafts, function (el) {
  el.addEventListener("click", saydaft);
});

function saydaft() {
  var audio = document.getElementById("daft");
  audio.play();
}
//-------------------------------------------------------------------------------
var blues = document.getElementsByClassName("blue");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(blues, function (el) {
  el.addEventListener("click", sayblue);
});

function sayblue() {
  var audio = document.getElementById("blue");
  audio.play();
}
//---------------------------------------------------------------
var barbies = document.getElementsByClassName("barbie");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(barbies, function (el) {
  el.addEventListener("click", saybarbie);
});

function saybarbie() {
  var audio = document.getElementById("barbie");
  audio.play();
}
//---------------------------------------------------------------
var sandstorms = document.getElementsByClassName("sandstorm");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(sandstorms, function (el) {
  el.addEventListener("click", saysandstorm);
});

function saysandstorm() {
  var audio = document.getElementById("sandstorm");
  audio.play();
}
//---------------------------------------------------------------
var takes = document.getElementsByClassName("take");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(takes, function (el) {
  el.addEventListener("click", saytake);
});

function saytake() {
  var audio = document.getElementById("take");
  audio.play();
}
//---------------------------------------------------------------
var plumas = document.getElementsByClassName("pluma");

// Array.prototype.forEach.call(kwaks, function(el) {
//     el.addEventListener("click", sayKwak);
// });
// Or
[].forEach.call(plumas, function (el) {
  el.addEventListener("click", saypluma);
});

function saypluma() {
  var audio = document.getElementById("pluma");
  audio.play();
}
//---------------------------------------------------------------
